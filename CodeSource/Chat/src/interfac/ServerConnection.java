package interfac;
import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ServerConnection extends Remote {

	void connect(String pseudo, String password, ClientConnection cltConnection, ClientManager cltManager) throws RemoteException;

	ClientConnection getClient(String to) throws RemoteException;
	
	String disconnect(ClientManager manager) throws RemoteException;
}
