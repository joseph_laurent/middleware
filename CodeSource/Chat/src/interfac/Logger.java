package interfac;

public class Logger {
	private Logger() {
	}
	
	public static void error (String error) {
		log("[error] : " + error);
	}
	
	public static void log(String msg) {
		System.out.println(msg);
	}
}
