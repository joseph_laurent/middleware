package interfac;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

public interface ClientManager extends Remote {

	void initClients(List<String> connectedClients) throws RemoteException;

	void addClient(String client) throws RemoteException;

	void remClient(String client) throws RemoteException;

	void showClients() throws RemoteException;
	
	void sendMessageCmd(ServerConnection serverConnection) throws RemoteException;

	String getPseudo() throws RemoteException;
	
	void setLog(String pseudo) throws RemoteException;

	boolean isConnected() throws RemoteException;

	void addMsgBox(MessageBox msgBox) throws RemoteException;

	void sendMessage(String pseudo2, String msg) throws RemoteException;
	
	MessageBox getMsgBox(String to) throws RemoteException;
}
