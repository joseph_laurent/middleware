package interfac;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ClientConnection extends Remote {

	MessageBox connect(MessageBox msgBox) throws RemoteException;

}