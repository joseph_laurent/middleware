package interfac;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

public interface MessageBox extends Remote {

	String from() throws RemoteException;

	String to() throws RemoteException;

	void receive(String msg) throws RemoteException;

	List<String> getMsgs() throws RemoteException;
}
