package test;

import static org.junit.jupiter.api.Assertions.*;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.List;

import org.junit.jupiter.api.Test;

import clients.ClientManagerImpl;
import interfac.ClientManager;
import interfac.Logger;
import interfac.MessageBox;
import interfac.ServerConnection;
import server.ClientModele;
import server.ListClients;

class TestAll {

	static final String PSEUDO = "Toto";
	static final String PW = "pw1";
	
	static final String PSEUDO2 = "Titi";
	static final String PW2 = "pw2";
	static final String MSG = "Bonjour";
	
	static final String PSEUDO3 = "abcdefghijklmnopqrstuvwxyz999999";
	static final String PW3 = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
	
	ClientManager cltManager;
	ClientManager cltManager2;
	
	ClientModele c = new ClientModele(PSEUDO3, PW3);

	@Test
	void testClientManagerImplStringString() {		
		//Test de connexion
		boolean success = false;
		try {
			cltManager = new ClientManagerImpl(PSEUDO, PW);
			success = cltManager.isConnected();
		} catch (RemoteException e) {
			Logger.error(e.getMessage());
		}
		assertTrue(success);
	}
	
	@Test
	void testDisconnect() throws RemoteException, NotBoundException {
		Registry reg = LocateRegistry.getRegistry(2000);
		ServerConnection serverConnection = (ServerConnection) reg.lookup("connection");
		cltManager = new ClientManagerImpl(PSEUDO, PW);
		serverConnection.disconnect(cltManager);
		assertNull(serverConnection.getClient(cltManager.getPseudo()));
	}
	
	@Test
	void testMessageBox() throws RemoteException {
		boolean success = false;
		try {
			cltManager = new ClientManagerImpl(PSEUDO, PW);
			cltManager2 = new ClientManagerImpl(PSEUDO2, PW2);
			cltManager.sendMessage(PSEUDO2, MSG);
			if (cltManager2.isConnected()) {
				MessageBox toMsgBox = cltManager.getMsgBox(PSEUDO2);
				
				List<String> allMsgs = toMsgBox.getMsgs();
				
				String msgReceive = allMsgs.get(allMsgs.size() - 1);
				
				success = MSG.equals(msgReceive);
			}
		} catch (RemoteException e) {
			Logger.error(e.getMessage());
		}
		assertTrue(success);
	}
	
	@Test
	void testIs() {
		assertTrue(c.is(c.getPseudo()));
	}

	@Test
	void testGetClient() {
		if (ListClients.getInstance().getClient(PSEUDO3) != null) {
			fail("Client already exist or method failed");
		}
		
		ListClients.getInstance().addClient(PSEUDO3, PW3);
		
		assertNotNull(ListClients.getInstance().getClient(PSEUDO3));
	}
}
