package server;
import java.rmi.RemoteException;

import interfac.ClientConnection;
import interfac.ClientManager;

public class ClientModele {
	private String pseudo;
	private String password;
	private boolean connected;
	private ClientConnection connection;
	private ClientManager manager;
	
	public ClientModele(String pseudo, String password) {
		this.pseudo = pseudo;
		this.password = password;
		connected = false;
	}
	
	public String getPseudo() {
		return this.pseudo;
	}
	
	public boolean isConnected() {
		return connected;
	}
	
	ClientConnection getConnection() {
		return this.connection;
	}
	
	ClientManager getManager() {
		return this.manager;
	}

	public boolean connect(String pw, ClientConnection cltConnection, ClientManager cltManager) throws RemoteException {
		if (pw.equals(this.password)) {
			this.connection = cltConnection;
			this.manager = cltManager;
			cltManager.setLog(pseudo);
			cltManager.initClients(ListClients.getInstance().getConnectedClients());
			this.connected = true;
			return true;
		}
		return false;
	}

	public void disconnect() {
		this.connection = null;
		this.manager = null;
		this.connected = false;
	}

	public boolean is(String pseudo) {
		return this.pseudo.equals(pseudo);
	}
}
