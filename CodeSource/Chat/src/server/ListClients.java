package server;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import interfac.ClientConnection;
import interfac.ClientManager;

public class ListClients {
	List<ClientModele> clients = new ArrayList<>() ;
	private static final String SUCCESS_DECONNECTION_MSG = "D�connection r�ussi";
	private static final String DECONNECTION_FAILED_MSG = "D�connection �chou�";
	
	/** Constructeur priv� */
	private ListClients()
	{
		//Cr�ation de clients
		clients.add(new ClientModele("Toto", "pw1"));
		clients.add(new ClientModele("Titi", "pw2"));
		clients.add(new ClientModele("Client1", "0000"));
		clients.add(new ClientModele("Client2", "0000"));
		clients.add(new ClientModele("Client3", "0000"));
		clients.add(new ClientModele("Client4", "0000"));
		clients.add(new ClientModele("Client5", "0000"));
	}
 
	/** Instance unique pr�-initialis�e */
	private static ListClients instance = new ListClients();
 
	/** Point d'acc�s pour l'instance unique du singleton */
	public static ListClients getInstance()
	{	
		return instance;
	}
	
	public ClientModele getClient(String pseudo) {
		for (ClientModele client : clients) {
			if (client.is(pseudo)) {
				return client;
			}
		}
		return null;
	}
	
	public void addClient(String pseudo, String pw) {
		clients.add(new ClientModele(pseudo, pw));
	}
	
	public void connect(String pseudo, String password, ClientConnection cltConnection, ClientManager cltManager) throws RemoteException 
	{
		ClientModele client = getClient(pseudo);
		if (client != null && client.connect(password, cltConnection, cltManager)) {
			for (ClientModele ct : clients) {
				if (ct.isConnected()) {
					ct.getManager().addClient(pseudo);
				}
			}
		}
	}

	public String disconnect(ClientManager manager) throws RemoteException {
		ClientModele client = getClient(manager.getPseudo());
		if (client != null) {
			client.disconnect();
			for (ClientModele ct : clients) {
				if (ct.isConnected()) {
					ct.getManager().remClient(manager.getPseudo());
				}
			}
			return SUCCESS_DECONNECTION_MSG;
		}
		return DECONNECTION_FAILED_MSG;
	}
	
	public String[] getClients() {
		String[] clientsPseudo = new String[clients.size()];
		for (int i = 0; i < clients.size(); i++) {
			clientsPseudo[i] = clients.get(i).getPseudo();
		}
		return clientsPseudo;
	}
	
	public List<String> getConnectedClients() {
		List<String> clientsPseudo = new ArrayList<>();
		for (int i = 0; i < clients.size(); i++) {
			if (clients.get(i).isConnected()) {
				clientsPseudo.add(clients.get(i).getPseudo());
			}
		}
		return clientsPseudo;
	}
}
