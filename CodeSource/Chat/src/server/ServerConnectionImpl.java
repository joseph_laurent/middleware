package server;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import interfac.ServerConnection;
import interfac.ClientConnection;
import interfac.ClientManager;

public class ServerConnectionImpl  extends UnicastRemoteObject implements ServerConnection {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4940812017671111180L;

	protected ServerConnectionImpl() throws RemoteException {
		super();
	}
	
	@Override
	public void connect(String pseudo, String password, ClientConnection cltConnection, ClientManager cltManager) throws RemoteException {
		ListClients.getInstance().connect(pseudo, password, cltConnection, cltManager);
	}

	@Override
	public ClientConnection getClient(String to) throws RemoteException {
		ClientModele client = ListClients.getInstance().getClient(to);
		if (client != null && client.isConnected()) {
			return client.getConnection();
		}
		return null;
	}

	@Override
	public String disconnect(ClientManager manager) throws RemoteException {
		return ListClients.getInstance().disconnect(manager);
	}
}
