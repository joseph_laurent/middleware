package server;
//Executer rmiregistry 2000 dans le repertoire bin avec CMD

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import interfac.Logger;
import interfac.ServerConnection;

public class Server {

	public static void main(String[] args) {
		try {
			ServerConnection connect = new ServerConnectionImpl();
			
			Registry reg = LocateRegistry.getRegistry(2000);
			
			reg.rebind("connection", connect);
			
			Logger.log("Server online");			
		} catch (RemoteException e) {
			Logger.error(e.getMessage());
		}
	}

}
