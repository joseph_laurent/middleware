package clients;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import interfac.ClientConnection;
import interfac.ClientManager;
import interfac.Logger;
import interfac.MessageBox;
import interfac.ServerConnection;

public class ClientManagerImpl  extends UnicastRemoteObject implements ClientManager {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2471576066598974805L;
	private List<String> clients;
	private transient List<MessageBox> messageBox = new ArrayList<>();
	private String pseudo;
	private boolean connected = false;
	private transient Scanner sc;
	private transient ServerConnection serverConnection;
	private transient ClientConnection connection;
	
	public ClientManagerImpl() throws RemoteException {
		super();
		sc = new Scanner(System.in);
		try {
			this.init();
			//Connection
			connect(serverConnection, connection);
			
			//Interpr�te les commandes client
			interpCommand(serverConnection);
		} catch (NotBoundException e) {
			Logger.error(e.getMessage());
		}
	}
	
	public ClientManagerImpl(String log, String pw) throws RemoteException {
		super();
		try {
			this.init();
			serverConnection.connect(log, pw, connection, this);
		} catch (NotBoundException e) {
			Logger.error(e.getMessage());
		}
	}
	
	@Override
	public boolean isConnected() {
		return this.connected;
	}

	@Override
	public void initClients(List<String> connectedClients) throws RemoteException {
		this.clients = connectedClients;
	}
	
	@Override
	public void addClient(String client) throws RemoteException {
		this.clients.add(client);
	}

	@Override
	public void remClient(String client) throws RemoteException {
		this.clients.remove(client);
	}
	
	@Override
	public void showClients() throws RemoteException {
		Logger.log("Clients connect�s : ");
		for (String clt : clients) {
			if (!pseudo.equals(clt)) {
				Logger.log(" - " + clt);
			}
		}
	}
	
	/**
	 * Permet � l'utilisateur d'interagir et d'envoyer des requ�tes
	 * @throws RemoteException
	 * @throws NotBoundException
	 */
	void init () throws RemoteException, NotBoundException {
		Registry reg = LocateRegistry.getRegistry(2000);
		connection = new ClientConnectionImpl(this);
		serverConnection = (ServerConnection) reg.lookup("connection");
	}
	
	private void connect(ServerConnection serverConnection, ClientConnection connection) throws RemoteException {
		Logger.log("Veuillez saisir votre login :");
		String log = sc.nextLine();
		Logger.log("Veuillez saisir votre mot de passe :");
		String pw = sc.nextLine();
		
		serverConnection.connect(log, pw, connection, this);
		
		while (!this.connected) {
			Logger.log("Connection �chou�");
			Logger.log("Veuillez saisir votre login :");
			log = sc.nextLine();
			Logger.log("Veuillez saisir votre mot de passe :");
			pw = sc.nextLine();
			serverConnection.connect(log, pw, connection, this);
		}
		Logger.log("Connection r�ussi");
	}
	
	private void interpCommand(ServerConnection serverConnection) throws RemoteException {
		Logger.log("Veuillez saisir une commande (commande help pour afficher les commandes possibles) :");
		String strScanner = sc.nextLine();
		
		//Interpretation des commandes client
		while (!strScanner.equals("disconnect")) {
			Logger.log("  ----------------------------------------------------");
			switch(strScanner) {
				case "help":
					Logger.log("Voici les diff�rentes commandes utilisables :");
					Logger.log(" - show clients : Affiche les clients connect�s");
					Logger.log(" - send : Pour envoy� un message");
					Logger.log(" - disconnect : Pour se d�connect�");
					break;
				case "show clients":
					this.showClients();
					break;
				case "send":
					//Envois du message
					this.sendMessageCmd(serverConnection);
					break;
				default:
					Logger.log("Commande inconnue.");
			}
			Logger.log("  ----------------------------------------------------");
			
			strScanner = sc.nextLine();
		}
		
		//D�connection
		Logger.log(serverConnection.disconnect(this));
	}

	@Override
	public void sendMessageCmd(ServerConnection serverConnection) throws RemoteException {
		Logger.log("Veuillez saisir le destinataire (un destinataire vide permet de retourner en arri�re).");
		String to = sc.nextLine();
		if ("".equals(to)) {
			return;
		}
		Logger.log("Veuillez saisir le message (un message vide permet de retourner en arri�re).");
		String msg = sc.nextLine();
		if ("".equals(msg)) {
			return;
		}
		
		sendMessage(to, msg);
	}
	
	public void sendMessage(String to, String msg) throws RemoteException {
		ClientConnection toCC = serverConnection.getClient(to);
		if (toCC == null) {
			return;
		}
			
		MessageBox toMsgBox = this.getMsgBox(to);		
		
		if (toMsgBox == null) {
			MessageBox msgBox = new MessageBoxImpl(pseudo, to);
			toMsgBox = toCC.connect(msgBox);
			addMsgBox(toMsgBox);
		}
		
		toMsgBox.receive(msg);
	}
	
	@Override
	public void addMsgBox(MessageBox msgBox) throws RemoteException {
		this.messageBox.add(msgBox);
	}

	@Override
	public MessageBox getMsgBox(String to) throws RemoteException {
		for (MessageBox msgBox : messageBox) {
			if (to.equals(msgBox.from())) {
				return msgBox;
			}
		}
		return null;
	}

	@Override
	public void setLog(String pseudo) throws RemoteException {
		this.pseudo = pseudo;
		this.connected = true;
	}

	@Override
	public String getPseudo() throws RemoteException {
		return this.pseudo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((pseudo == null) ? 0 : pseudo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ClientManagerImpl other = (ClientManagerImpl) obj;
		if (pseudo == null) {
			if (other.pseudo != null)
				return false;
		} else if (!pseudo.equals(other.pseudo))
			return false;
		return true;
	}
}