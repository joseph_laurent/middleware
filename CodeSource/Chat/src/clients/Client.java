package clients;

import java.rmi.RemoteException;

import interfac.Logger;
public class Client {

	public static void main(String[] args) {
		try {
			new ClientManagerImpl();
		} catch (RemoteException e) {
			Logger.error(e.getMessage());
		}
	}
}
