package clients;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import interfac.ClientConnection;
import interfac.ClientManager;
import interfac.MessageBox;

public class ClientConnectionImpl  extends UnicastRemoteObject implements ClientConnection {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7433217672605655640L;
	private transient ClientManager manager;

	public ClientConnectionImpl(ClientManager manager) throws RemoteException {
		super();
		this.manager = manager;
	}

	@Override
	public MessageBox connect(MessageBox emitterMsgBox) throws RemoteException {
		manager.addMsgBox(emitterMsgBox);
		return new MessageBoxImpl(emitterMsgBox.to(), emitterMsgBox.from());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((manager == null) ? 0 : manager.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ClientConnectionImpl other = (ClientConnectionImpl) obj;
		if (manager == null) {
			if (other.manager != null)
				return false;
		} else if (!manager.equals(other.manager))
			return false;
		return true;
	}
}