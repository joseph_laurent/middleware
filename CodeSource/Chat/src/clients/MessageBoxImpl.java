package clients;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.List;

import interfac.Logger;
import interfac.MessageBox;

public class MessageBoxImpl  extends UnicastRemoteObject implements MessageBox {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String from;
	private List<String> receiveMsg = new ArrayList<>();
	private String to;

	protected MessageBoxImpl(String from, String to) throws RemoteException {
		super();
		this.from = from;
		this.to = to;
	}
	
	@Override
	public String from() {
		return this.from;
	}
	
	@Override
	public String to() {
		return this.to;
	}
	
	@Override
	public List<String> getMsgs() {
		return this.receiveMsg;
	}
	
	@Override
	public void receive(String msg) {
		receiveMsg.add(msg);
		this.show();
	}
	
	private void show() {
		Logger.log("  ----------------------------------------------------------  ");
		Logger.log("Old message from " + to + " :");
		for (int i = 0; i < (receiveMsg.size() - 1); i++) {
			Logger.log("	- " + receiveMsg.get(i));
		}
		Logger.log(to + " send you a new message :");
		Logger.log("	" + receiveMsg.get(receiveMsg.size() - 1));
		Logger.log("  ----------------------------------------------------------  ");
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((from == null) ? 0 : from.hashCode());
		result = prime * result + ((to == null) ? 0 : to.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		MessageBoxImpl other = (MessageBoxImpl) obj;
		if (from == null) {
			if (other.from != null)
				return false;
		} else if (!from.equals(other.from))
			return false;
		if (to == null) {
			if (other.to != null)
				return false;
		} else if (!to.equals(other.to))
			return false;
		return true;
	}
}
